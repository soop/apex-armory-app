const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

/**
 * takes the platform and gamertag and returns a data object with all relevant information
 */
router.get('/:platform/:gamertag', async(req, res) => {

  // try to fetch
   try {
     const headers = {
       'TRN-Api-Key': process.env.TRACKER_API_KEY
     };

     // params
     const {platform, gamertag} = req.params;

     // use node-fetch to async fetch from tracker.gg based on params
     const response = await fetch(`${process.env.TRACKER_API_URL}/profile/${platform}/${gamertag}`, {
       headers
     });

     // set data as the reponse
     const data = await response.json();

     // check if data contains .errors as a result of incorrect gamertag and return 404
     if (data.errors && data.errors.length > 0) {
       return res.status(404).json({
         message: 'Profile not found'
       })
     };

     // return data wrapped as json
     res.json(data);

   } catch(err) {
     // console log the error and send 500 - server error
     console.error(err);
     res.status(500).json({
       message: 'Server Error'
     })
   }


});

// export to express as a module
module.exports = router;
