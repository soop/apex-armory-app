const express = require("express");
const morgan = require("morgan");
const dotenv = require("dotenv");

// load enviroment conf
dotenv.config({ path: `./config.env` });

const app = express();

// logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// load routes
app.use('/api/v1/profile', require('./routes/profile'));

// pings the server to check if it is alive.
app.get('/ping', (req, res) => {
  res.status(200).json({
    message: 'pong'
  })
});


const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log(`App is running in ${process.env.NODE_ENV} on port: ${port}`);
});
